require 'daemons'
require "action_mailer/railtie"
require "net/https"
require "uri"

admin_email = 'alert@pokupon.ua'
site_list = ['https://partner.pokupon.ua', 'https://pokupon.ua']

ActionMailer::Base.smtp_settings = { 
  address: 'localhost',
  port: 1025
}

class ApplicationMailer < ActionMailer::Base
  default from: '<noreply@.pokupon.ua>'
end

class ReportMailer < ApplicationMailer

  def status_email(admin_email, res_code)
    mail(
      to: admin_email, 
      subject: 'status ' + res_code, 
      body: '', 
      content_type: "text/html"
    )
  end
end

Daemons.run_proc('myproc.rb') do

  statuses = {}
  site_list.each do |k|
    statuses[k] = '200'
  end

  loop do
    statuses.each do |k, v|
      uri = URI.parse(k)
      http = Net::HTTP.new(uri.host, uri.port)
      http.use_ssl = true

      request = Net::HTTP::Get.new(uri.request_uri)
      res = http.request(request)
      if res.code != v && statuses[k] == '200' || res.code == '200' && statuses[k] != '200'
        ReportMailer.status_email(admin_email, res.code).deliver_now 
      end
      statuses[k] = res.code
    end
    sleep(60)
  end

end