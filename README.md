Script for checking the site availability

To run the script:
  - in the terminal go to the project directory 'pokupon';
  - execute 'ruby myserver_control.rb run' command. 
  - use Mailcatcher to test the sending of a message

Ruby Version 2.3.0